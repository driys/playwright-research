import { test, expect } from "@playwright/test";

test.describe.parallel("API Testing", () => {
  const baseURL = "http://admss-api.stagingapps.net/";

  //Faskes List
  test("Get Faskes List", async ({ request }) => {
    const response = await request.post(`${baseURL}/api/v3/FaskesList`, {
      data: {
        search: "",
        limit: 2,
        order: null,
        sort: null,
        coordinate_user: [-6.934924408727809, 107.53824969070268],
        partner_name: "ihc"
      },
    });
    expect(response.status()).toBe(200);
    expect(response.ok()).toBeTruthy(); 
  });

  //Faskes Detail
  test("Get Faskes Detail", async ({ request }) => {
    const response = await request.post(`${baseURL}/api/v3/FaskesDetail`, {
      params: {
        adm_provider_code: "MK002",
        provider_id: 55
      },
      data: {
        coordinate_user: [-6.935755840788698, 107.57782902229822]
      },
    });
    expect(response.status()).toBe(200);
    expect(response.ok()).toBeTruthy();
    // console.log(await response.headers());
    // console.log(await response.json());
  });

  
});
