import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {
  await page.goto('https://binus-cms.stagingapps.net/');
  await page.goto('https://binus-cms.stagingapps.net/auth/login');
  await page.pause();
  await page.getByPlaceholder('Enter Username / Email').click();
  await page.getByPlaceholder('Enter Username / Email').fill('test123');
  await page.getByPlaceholder('Password').click();
  await page.getByPlaceholder('Password').fill('test123');
  await page.getByRole('button', { name: 'Login' }).click();
  await page.getByRole('button', { name: 'icon-menuContent Management' }).click();
  await page.getByText('Banner Management').click();
  await page.getByRole('button', { name: 'Add Banner' }).click();
});