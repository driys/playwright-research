import { test, expect } from '@playwright/test';
import LoginPage from '../../../pages/dashboard/login';

test('test', async ({ page }) => {

  const Login = new LoginPage(page)

  await Login.redirectToLoginPage();
  await Login.enterEmail('gerywahyunugraha@gmail.com');
  await Login.enterPassword('ger1wahyunugraha');
  await Login.clickLogin();
 
  const locator = page.locator("xpath=//img[@alt='Payable logo']");
  await expect(locator).toBeVisible();
});