import { test, expect } from '@playwright/test';
import MockPhone from '../../../pages/shopify/mock_phonenumber';
import SelectItem from '../../../pages/shopify/select_item';
import SelectPayment from '../../../pages/shopify/select_payment';

test.describe.parallel('Create checkout and payment', () => {
    
    test('Payment using Credit Card or Debit', async ({ page }) => {
      
      const Payment = new SelectPayment(page)
      const Mock = new MockPhone(page)
      const Select = new SelectItem(page)
      
      // Page Object Model Typescript for Select Item
      await page.pause();
      await Select.buyItNow();
      // await Select.addToCart();

      // Page Object Model Typescript for Mock Phone Number
      await Mock.enterPhoneNumber("628100000000");
      await Mock.clickFieldEmail();
      await Mock.enterOTP("1", "2", "3", "4");
      await Mock.selectCourier();

      // await page.frameLocator('iframe[name="sample-inline-frame"]').getByRole('paragraph').filter({ hasText: 'Rp200.000' }).click();
      await page.frameLocator('iframe[name="sample-inline-frame"]').getByRole('button', { name: 'Bayar' }).click();

      // Page Object Model Typescript for Select Payment Method
      await Payment.paymentCCOrDebit("5573381011111101", "0128", "123");
    });

    test('Payment using QRIS', async ({ page }) => {
      
      const Payment = new SelectPayment(page)
      const Mock = new MockPhone(page)
      const Select = new SelectItem(page)
      
      // Page Object Model Typescript for Select Item
      await page.pause();
      await Select.buyItNow();
      // await Select.addToCart();

      // Page Object Model Typescript for Mock Phone Number
      await Mock.enterPhoneNumber("628100000000");
      await Mock.clickFieldEmail();
      await Mock.enterOTP("1", "2", "3", "4");
      await Mock.selectCourier();

      await page.frameLocator('iframe[name="sample-inline-frame"]').getByRole('button', { name: 'Bayar' }).click();

      // Page Object Model Typescript for Select Payment Method
      await Payment.paymentQRIS();
    });

    test('Payment using Pay Later', async ({ page }) => {
      
      const Payment = new SelectPayment(page)
      const Mock = new MockPhone(page)
      const Select = new SelectItem(page)
      
      // Page Object Model Typescript for Select Item
      await page.pause();
      // await Select.buyItNow();
      await Select.addToCart();

      // Page Object Model Typescript for Mock Phone Number
      await Mock.enterPhoneNumber("628100000000");
      await Mock.clickFieldEmail();
      await Mock.enterOTP("1", "2", "3", "4");
      await Mock.selectCourier();

      await page.frameLocator('iframe[name="sample-inline-frame"]').getByRole('button', { name: 'Bayar' }).click();

      // Page Object Model Typescript for Select Payment Method
      await Payment.PaymentPaylater('0812000000', '123456', '00000'); //Success 
      // await Payment.PaymentPaylater('0812000000', '111111', '00000'); //Failed
    });

    test('Payment using Virtual Account', async ({ page }) => {
      
      const Payment = new SelectPayment(page)
      const Mock = new MockPhone(page)
      const Select = new SelectItem(page)
      
      // Page Object Model Typescript for Select Item
      await page.pause();
      await Select.buyItNow();
      // await Select.addToCart();

      // Page Object Model Typescript for Mock Phone Number
      await Mock.enterPhoneNumber("628100000000");
      await Mock.clickFieldEmail();
      await Mock.enterOTP("1", "2", "3", "4");
      await Mock.selectCourier();

      await page.frameLocator('iframe[name="sample-inline-frame"]').getByRole('button', { name: 'Bayar' }).click();

      // Page Object Model Typescript for Select Payment Method
      await Payment.PaymentVA();
    });
});