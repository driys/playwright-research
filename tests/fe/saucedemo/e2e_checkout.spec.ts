import { test, expect } from '@playwright/test';

test('e2e checkout saucedemo', async ({ page }) => {
  // Redirect to saucedemo login page
  await page.goto('https://www.saucedemo.com/');

  // Show Codegen Controller
  // await page.pause();

  // Input Username
  await page.locator('[data-test="username"]').click();
  await page.locator('[data-test="username"]').fill('standard_user');
  
  // Input Password
  await page.locator('[data-test="password"]').click();
  await page.locator('[data-test="password"]').fill('secret_sauce');
  
  // Press Login Button   
  await page.locator('[data-test="login-button"]').click();

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/Swag Labs/);

  // Check item price
  // await page.getByText('$15.99').first().click();

  // Check item price
  if (await page.$('text=$14.99')) {
      // Add to cart T-Shirt
      await page.locator('[data-test="add-to-cart-sauce-labs-bolt-t-shirt"]').click();
  }
  

  // Check item price
  await page.getByText('$29.99').click();
  
  // Add to cart Backpack
  await page.locator('[data-test="add-to-cart-sauce-labs-backpack"]').click();
  
  // Click checkout icon
  await page.locator('a').filter({ hasText: '2' }).click();
  await page.locator('[data-test="checkout"]').click();
  
  // Fill firstname
  await page.locator('[data-test="firstName"]').click();
  await page.locator('[data-test="firstName"]').fill('Haris');
  
  // Fill lastname
  await page.locator('[data-test="firstName"]').press('Tab');
  await page.locator('[data-test="lastName"]').fill('Abdullah');

  // Fill postal code
  await page.locator('[data-test="lastName"]').press('Tab');
  await page.locator('[data-test="postalCode"]').fill('123456');

  // Click continue to checkout
  await page.locator('[data-test="continue"]').click();
  
  // Expect total price
  await page.getByText('Total: $49.66').click();
  
  // Click finish checkout and back to home
  await page.locator('[data-test="finish"]').click();
  await page.locator('[data-test="back-to-products"]').click();
});