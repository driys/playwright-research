import { test, expect } from "@playwright/test";

test("login saucedemo", { tag: "@fast" }, async ({ page }) => {
  // Redirect to saucedemo login page
  await page.goto("https://www.saucedemo.com/");

  // Show Codegen Controller
  await page.pause();

  // Input Username
  await page.locator('[data-test="username"]').click();
  await page.locator('[data-test="username"]').fill("standard_user");

  // Input Password
  await page.locator('[data-test="password"]').click();
  await page.locator('[data-test="password"]').fill("secret_sauce");

  // Press Login Button
  await page.locator('[data-test="login-button"]').click();

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/Swag Labs/);
});
