import { chromium, Browser } from 'playwright';

(async () => {
  // Launch the browser
  const browser: Browser = await chromium.launch({ headless: false });
  
  // Create a new page
  const page = await browser.newPage();

  // Navigate to the webpage
  await page.goto('https://zzzscore.com/1to50/en');

  // Map indexes and numbers
  const indexes: number[] = [];
  for (let i = 1; i <= 25; i++) {
    const element = await page.$(`#grid > div:nth-child(${i})`);
    const index = await element?.textContent();
    console.log(`Found ${index} in position ${i}`);
    indexes[parseInt(index!)] = i;
  }

  // Click the first 25 elements and update indexes for the second round
  for (let i = 1; i <= 25; i++) {
    const index = indexes[i];
    const element = await page.$(`#grid > div:nth-child(${index}) > .box`);
    await element?.click();
    await page.waitForTimeout(200); // Good enough solution for the minimum time it takes to change to the new number
    let index2 = i;
    while (index2 === i || index2 === undefined) {
      // Waiting for the number to change
      const element2 = await page.$(`#grid > div:nth-child(${index})`);
      const textContent = await element2?.textContent() ?? '';
      index2 = parseInt(textContent);
    }
    console.log(`Clicked element ${i} and turned to ${index2}`);
    indexes[index2] = index!;
}


  // Click the other 25
  for (let i = 26; i <= 50; i++) {
    const index = indexes[i];
    const element = await page.$(`#grid > div:nth-child(${index}) > .box`);
    await element!.click();
    await page.waitForTimeout(200);
    console.log(`Clicked element ${i} on position ${index}`);
  }

  // Get resolved time
  const time = await page.textContent('#result > div.resultContent > strong');
  console.log(`Resolved in ${time}`);

  // Save screenshot
  await page.screenshot({ path: 'result.png' });

  // Close the browser
  await browser.close();
})();
