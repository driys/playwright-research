// loginSteps.ts
import { Given, When, Then, After, setDefaultTimeout  } from '@cucumber/cucumber';
import { chromium, Browser, Page, expect } from '@playwright/test';

let browser: Browser;
let page: Page;

setDefaultTimeout(10 * 1000); 

Given('I am on the OpenMRS login page', async () => {
  browser = await chromium.launch({headless:false}); // Headless by default
  const context = await browser.newContext();
  page = await context.newPage();
  await page.goto('https://demo.openmrs.org/openmrs/login.htm', { waitUntil: 'networkidle' });
});

When('I enter a valid username {string} and password {string}', async (username: string, password: string) => {
  await page.locator('[name="username"]').first().fill(username);
  await page.locator('[name="password"]').first().fill(password);
});

When('I select Location', async () => {
  await page.locator('[id="Inpatient Ward"]').click();
});

When('I click the "Login" button', async () => {
  await page.locator('[id="loginButton"]').click();
});

Then('I should be logged in successfully', async () => {
  // Add assertions or verifications here
  // For example, check for the presence of an element on the dashboard page
  const textToVerify = 'Logged in as Super User (admin) at Inpatient Ward.'
  // Wait for an element containing the specified text to become visible
  await page.waitForSelector(`:text("${textToVerify}")`, { state: 'visible' });

  // If the above line doesn't throw an error, it means the text is visible
  console.log(`The text "${textToVerify}" is visible on the page.`);
});

// Ensure that the browser is always closed, even in case of failure
After(async () => {
  if (browser) {
    await browser.close();
  }
});
