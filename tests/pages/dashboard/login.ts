import { Page } from "@playwright/test";
export default class LoginPage{

    constructor(public page: Page){ }

        async redirectToLoginPage(){
            await this.page.goto('https://7745292268.dashboard.payable.id/login');
        }

        async enterEmail(email: string){
            await this.page.getByPlaceholder('Email address').click();
            await this.page.getByPlaceholder('Email address').fill(email);
        }

        async enterPassword(password: string){
            await this.page.getByPlaceholder('Password').click();
            await this.page.getByPlaceholder('Password').fill(password);
        }

        async clickLogin(){
            await this.page.getByRole('button', { name: 'Sign in' }).click();
        }

        async login(email: string, password: string){
            await this.page.goto('https://7745292268.dashboard.payable.id/login');
            await this.page.getByPlaceholder('Email address').click();
            await this.page.getByPlaceholder('Email address').fill(email);
            await this.page.getByPlaceholder('Password').click();
            await this.page.getByPlaceholder('Password').fill(password);
            await this.page.getByRole('button', { name: 'Sign in' }).click();
        }
}
