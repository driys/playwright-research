import { Page, chromium, expect } from "@playwright/test";
import {CopyToClipboard} from 'react-copy-to-clipboard';

export default class SelectPayment{

    constructor(public page: Page){ }

        async paymentCCOrDebit(ccordebit: string, expired: string, cvv: string){
            // const iframe = await this.page.frameLocator('iframe[name="sample-inline-frame"]').frameLocator('iframe[name="occ-frame-doku-cc"]')
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').frameLocator('iframe[name="occ-frame-doku-cc"]').getByLabel('Nomor Kartu Kredit/Debit  *').fill(ccordebit);
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').frameLocator('iframe[name="occ-frame-doku-cc"]').getByPlaceholder('mm / yy').fill(expired);
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').frameLocator('iframe[name="occ-frame-doku-cc"]').getByPlaceholder('Contoh 123').fill(cvv);
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').frameLocator('iframe[name="occ-frame-doku-cc"]').getByRole('button', { name: 'BAYAR' }).click();
            // iframe.getByText('910153').click();
            // let otp = await this.page.locator('//*[@id="MerchatPaymentPage"]/table/tbody/tr[2]/td/strong/font').textContent();
            // console.log(otp);
            const otp1 = await this.page.frameLocator('iframe[name="sample-inline-frame"]').frameLocator('iframe[name="occ-frame-doku-cc"]').locator('xpath=/html/body/form/table/tbody/tr[2]/td/strong/font').textContent();
            console.log('OTP number = '+otp1);
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').frameLocator('iframe[name="occ-frame-doku-cc"]').locator('#otp').click();
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').frameLocator('iframe[name="occ-frame-doku-cc"]').locator('#otp').fill(''+otp1);
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').frameLocator('iframe[name="occ-frame-doku-cc"]').getByRole('button', { name: 'SUBMIT' }).click();
            // let handle = await this.page.locator('//*[@id="MerchatPaymentPage"]/table/tbody/tr[2]/td/strong/font').textContent();
        }

        async paymentQRIS(){         
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').locator('div').filter({ hasText: 'Kartu Debit/Kredit' }).nth(1).click();
            await this.page.waitForTimeout(2000);
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByText('Kartu Debit/Kredit').click();
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByText('QRIS/E-Wallet').click();
            // let random = await this.page.locator('//*[@id="__next"]/div[2]/div[1]/div/div[5]/div/ol/li[1]/p').textContent();
            let qrString = await this.page.frameLocator('iframe[name="sample-inline-frame"]').locator('xpath=//*[@id="occ-qr-string"]').textContent({timeout: 4000});
            // const qrstring = qr?.textContent();
            console.log('Text Content'+ ' = ' + qrString);
            // console.log(random);

            const browser = await chromium.launch();
            const context = await browser.newContext();
            const page1 = await context.newPage();
            await page1.goto('https://doku.com/testqr/');
            await page1.pause();
            await page1.getByRole('button', { name: 'Input QR' }).click();
            await page1.getByPlaceholder('QR Code String').click();
            await page1.getByPlaceholder('QR Code String').fill(''+qrString); // Dummy QR Code String
            await page1.getByText('Scan', { exact: true }).click();
        }

        async PaymentPaylater(phonenumber: string, password: string, otp: string){
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').locator('div').filter({ hasText: 'Kartu Debit/Kredit' }).nth(1).click();
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByText('Kartu Debit/Kredit').click();
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByText('Pay Later').click();
            const page1Promise = this.page.waitForEvent('popup');
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByRole('button', { name: 'Merchant photo' }).first().click();
            const page1 = await page1Promise;
            await page1.getByPlaceholder('Phone number').click();
            await page1.getByPlaceholder('Phone number').fill(phonenumber);
            await page1.getByPlaceholder('Password').click();
            await page1.getByPlaceholder('Password').fill(password);
            await page1.getByRole('button', { name: 'Login' }).click();
            await page1.waitForTimeout(3000);
            await page1.getByPlaceholder('OTP').click();
            await page1.getByPlaceholder('OTP').fill(otp);
            await page1.getByRole('button', { name: 'PAY NOW' }).click();
            await expect(page1.getByRole('heading', { name: 'Payment Success' })).toBeVisible();
            // await page1.getByRole('heading', { name: 'Payment Success' }).click();
            await page1.getByRole('button', { name: 'Payment Done' }).click();
            const page2Promise = page1.waitForEvent('popup');
            // await page1.waitForTimeout(2000);
            await page1.getByRole('button', { name: 'Lacak pengiriman' }).click();
            const page2 = await page2Promise;
            await page2.waitForTimeout(3000);
            await expect(page2.getByText('Menunggu Konfirmasi Pengiriman')).toBeVisible();
            // await page2.getByText('Menunggu Konfirmasi Pengiriman').click();
        }

        async PaymentVA() {
            // const browser = await chromium.launch();
            // const context = await browser.newContext();
            // const page1 = await context.newPage();

            // await this.page.waitForTimeout(4000);
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').locator('div').filter({ hasText: 'Kartu Debit/Kredit' }).nth(1).click();
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByText('Virtual Account').click();
            // await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByRole('button', { name: 'Merchant photo' }).nth(1).click(); // Mandiri VA
            // await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByRole('button', { name: 'Merchant photo' }).nth(2).click(); // BNI VA
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByRole('button', { name: 'Merchant photo' }).nth(3).click(); // BRI VA
            await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByRole('button', { name: 'Salin' }).click();

            if (await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByText('Virtual Account - Mandiri').isVisible()){
                await this.page.goto('https://sandbox.doku.com/integration/simulator/');
                // await page1.pause();
                await this.page.locator('#simulate-mandiri').click();
                await this.page.getByPlaceholder('8889933344445555').click();
            } else if (await this.page.frameLocator('iframe[name="sample-inline-frame"]').getByText('Virtual Account - BNI').isVisible()){
                await this.page.goto('https://sandbox.doku.com/integration/simulator/');
                // await this.page.pause();
                await this.page.locator('#simulate-bni').click();
                await this.page.getByPlaceholder('8889933344445555').click();
            } else {
                await this.page.goto('https://sandbox.doku.com/integration/simulator/');
                // await page1.pause();
                await this.page.locator('#simulate-bri').click();
                await this.page.getByPlaceholder('8889933344445555').click();
            }
            

            // let text
            // await navigator.clipboard.readText(text);
            const modifier = 'Control';
            await this.page.keyboard.press(`${modifier}+KeyV`);
            // await this.page.getByPlaceholder('8889933344445555').fill(text);
            await this.page.getByRole('button', { name: 'Inquiry' }).click();
            await this.page.getByRole('button', { name: 'Pay Now' }).click();
            await expect(this.page.getByRole('heading', { name: 'Payment Success' })).toBeVisible();
        }
}
